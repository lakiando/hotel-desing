//
//  RoomsController.swift
//  HotelDesing
//
//  Created by iosakademija on 12/15/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit

class RoomsController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
}


// MARK: Collection View Data Source

extension RoomsController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 88
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoomsCell",
                                                      for: indexPath)
        return cell
    }
}

extension RoomsController {
    
    @IBAction func didtapHomeController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
            
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    @IBAction func didTapRestorauntController(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "RestourantController") as? RestourantController else {
            
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    @IBAction func didtapRoomsController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "RoomsController") as? RoomsController else {
            
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
}

extension RoomsController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let layout = collectionViewLayout as! RoomsLayout
        
        let availableWidth = collectionView.bounds.size.width
        let columns = (availableWidth / 4 > 150) ? 4 : 2
        var itemTotalWidth = availableWidth - CGFloat(columns-1) * layout.minimumInteritemSpacing
        itemTotalWidth -= (layout.sectionInset.left + layout.sectionInset.right)
        
        let itemWidth = itemTotalWidth / CGFloat(columns)
        return CGSize(width: itemWidth, height: itemWidth)
        
    }
    
}

