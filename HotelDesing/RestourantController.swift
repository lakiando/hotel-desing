//
//  RestourantController.swift
//  HotelDesing
//
//  Created by iosakademija on 12/16/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit

class RestourantController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension RestourantController {
    
    @IBAction func didTapHomeController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
            
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    @IBAction func didTapRoomsController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "RoomsController") as? RoomsController else {
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    @IBAction func didTapResorauntController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "RestourantController") as? RestourantController else {
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    
}


extension RestourantController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 88
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RestorauntCell",
                                                      for: indexPath)
        return cell
    }
}

extension RestourantController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionViewLayout as! RestorauntLayout
        
        let aviableHeight = collectionView.bounds.size.height
        let availableWidth = collectionView.bounds.size.width
        let columns = 1
        var itemTotalHeigth = aviableHeight - CGFloat(columns-1) * layout.minimumLineSpacing
        var itemTotalWidth = availableWidth - CGFloat(columns-1) * layout.minimumInteritemSpacing
        itemTotalWidth -= (layout.sectionInset.left + layout.sectionInset.right)
        itemTotalHeigth -= (layout.sectionInset.top + layout.sectionInset.bottom)
        
        
        let itemWidth = itemTotalWidth / CGFloat(columns)
        let itemHeigth = itemTotalHeigth - itemWidth
        return CGSize(width: itemWidth, height: itemHeigth)
    }
}


