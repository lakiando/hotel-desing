//
//  HomeLayout.swift
//  HotelDesing
//
//  Created by iosakademija on 12/17/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit

class HomeLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit() {
        
        
        self.minimumInteritemSpacing = 1
        self.minimumLineSpacing = 1
        self.sectionInset = .zero
        self.scrollDirection = .vertical
        
    }
    
}
