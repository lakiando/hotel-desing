//
//  ViewController.swift
//  HotelDesing
//
//  Created by iosakademija on 12/14/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var leblName: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        
    }
}


extension ViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        let sections = 3
        
        
        return sections
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 100
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell",
                                                      for: indexPath)
        
        
        
        return cell
    }
}

extension ViewController {
    
    @IBAction func didTapRoomsController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "RoomsController") as? RoomsController else {
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    @IBAction func didTapRestourantController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "RestourantController") as? RestourantController else {
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    @IBAction func didTapHomeController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nc = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
            fatalError("Faild to create a istance of \(storyboard)")
        }
        
        show(nc, sender: self)
    }
    
    
}



extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionViewLayout as! HomeLayout
        
        let aviableHeight = collectionView.bounds.size.height
        let availableWidth = collectionView.bounds.size.width
        let columns = 1
        var itemTotalHeigth = aviableHeight - CGFloat(columns-1) * layout.minimumLineSpacing
        var itemTotalWidth = availableWidth - CGFloat(columns-1) * layout.minimumInteritemSpacing
        itemTotalWidth -= (layout.sectionInset.left + layout.sectionInset.right)
        itemTotalHeigth -= (layout.sectionInset.top + layout.sectionInset.bottom)
        
        
        let itemWidth = itemTotalWidth / CGFloat(columns)
        let itemHeigth = itemTotalHeigth - itemWidth
        return CGSize(width: itemWidth, height: itemHeigth)
    }
    
}









