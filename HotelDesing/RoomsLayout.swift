//
//  RoomsLayout.swift
//  HotelDesing
//
//  Created by iosakademija on 12/17/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit

class RoomsLayout: UICollectionViewFlowLayout {

    
    override init() {
        super.init()
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit() {
        
        self.itemSize = CGSize(width: 150, height: 150)
        self.minimumInteritemSpacing = 1
        self.minimumLineSpacing = 1
        self.sectionInset = .zero
        self.scrollDirection = .vertical
        
    }
}
